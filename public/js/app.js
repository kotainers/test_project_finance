/**
 * Created by kotainer on 02.11.2016.
 */

var month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
    "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
angular
    .module( 'MyFinance', ['tableSort', 'ui.bootstrap', 'chart.js'] )
    .controller( "Table", function ($scope, wsService)  {
        /*ws.onmessage = function (event) {
            var msg = JSON.parse(event.data);
            if (msg.act === 'fullList') {
                if (!$scope.$$phase) $scope.$apply(function () {
                    $scope.items = msg.data;
                    var month_result = [];
                    
                    $scope.items.forEach(function (item) {
                        var m = new Date(item.date).getUTCMonth()+1;

                        if(m in month_result) {
                            month_result[m] = {
                                m: m,
                                income: month_result[m].income + item.income,
                                outcome: month_result[m].outcome + item.outcome,
                                profit: month_result[m].profit + item.profit
                            };
                        }
                        else {
                            month_result[m] = {
                                m: m,
                                income: item.income,
                                outcome: item.outcome,
                                profit: item.profit
                            };
                        }

                        $scope.itog.income += item.income;
                        $scope.itog.outcome += item.outcome;
                        $scope.itog.profit += item.profit;

                    });

                    //Чистим пустые элементы
                    month_result = month_result.filter(function(e){return e});

                    month_result.forEach(function (item) {
                        $scope.labels.push(month[item.m-1]);
                        $scope.data[0].push(item.income);
                        $scope.data[1].push(item.outcome);
                        $scope.data[2].push(item.profit);
                    });
                });
            }
        };*/

        $scope.$on('tablesort:sortOrder', (event, sortOrder) => {

        });

        //Обнуляем переменные
        $scope.itog = {};
        $scope.itog.income = 0;
        $scope.itog.outcome = 0;
        $scope.itog.profit = 0;

        $scope.labels = [];
        $scope.series = ['Поступление', 'Списание', 'Прибыль'];
        $scope.data = [
            [],
            [],
            []
        ];

        $scope.datasetOverride = [
            { yAxisID: 'y-axis-1' }
        ];
        $scope.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }
                ]
            }
        }

    })
    .service('wsService', function($rootScope) {
        var ws = new WebSocket("ws://localhost:8987");

        ws.onopen = function () {
            ws.send(JSON.stringify({
                act: 'getAll'
            }));
        };

        ws.onmessage = function (event) {
            var msg = JSON.parse(event.data);
            if (msg.act === 'fullList') {
                $rootScope.$apply(function(){
                    $rootScope.items = msg.data;
                });
            }
        };

       self.send = function(act,data) {
            ws.send(JSON.stringify({
                act: act,
                data: data
            }));
       };
    })
    .service('plotChart', function($rootScope) {

    })
;

angular
    .module('MyFinance')
    .config(['tableSortConfigProvider', function(tableSortConfigProvider){
        var pagerString = "<div class='text-left'>";
        pagerString += "<small class='text-muted'>" +
            "Показано {{CURRENT_PAGE_RANGE}} {{FILTERED_COUNT === 0 ? '' : 'из'}} ";
        pagerString += "<span ng-if='FILTERED_COUNT === TOTAL_COUNT'>" +
                "{{TOTAL_COUNT | number}} " +
                "{{TOTAL_COUNT === 1 ? ITEM_NAME_SINGULAR : ITEM_NAME_PLURAL}}" +
            "</span>";
        pagerString += "<span ng-if='FILTERED_COUNT !== TOTAL_COUNT'>" +
                "{{FILTERED_COUNT | number}} " +
                "{{FILTERED_COUNT === 1 ? ITEM_NAME_SINGULAR : ITEM_NAME_PLURAL}} " +
                "(filtered from {{TOTAL_COUNT | number}})" +
            "</span>";
        pagerString += "</small>&nbsp;";
        pagerString += "<uib-pagination style='vertical-align:top; margin-top:0;' " +
            "ng-if='ITEMS_PER_PAGE < TOTAL_COUNT' ng-model='CURRENT_PAGE_NUMBER' " +
            "total-items='FILTERED_COUNT' items-per-page='ITEMS_PER_PAGE' " +
            "max-size='5' force-ellipses='true'></uib-pagination>";
        pagerString += "&nbsp;";
        pagerString += "<div class='form-group' style='display:inline-block;'>" +
            "<select class='form-control' ng-model='ITEMS_PER_PAGE' " +
            "ng-options='opt as (\"по \" + opt + \" на странице\") " +
            "for opt in PER_PAGE_OPTIONS'></select></div>";
        pagerString += "</div>";
        tableSortConfigProvider.paginationTemplate = pagerString;
    }
    ]);