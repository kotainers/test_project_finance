/**
 * Created by kotainer on 02.11.2016.
 */
var ws = new WebSocket("ws://localhost:8987");
if (!("Notification" in window)) {
    alert("Ваш браузер не поддерживает HTML5 Notifications");
}
else if (Notification.permission === 'default') {
    Notification.requestPermission(function (permission) {
        if(!('permission' in Notification)) {
            Notification.permission = permission;
        }
    });
}
angular
    .module( 'MyFinanceAdd', ['ui.bootstrap'] )
    .controller( "Add", function ($scope)  {

        $scope.type = '0';
        $scope.price = '';
        $scope.date = '';

        $scope.addNew = function () {
          ws.send(
              JSON.stringify(
                  {
                      act: "addNew",
                      date: $scope.date,
                      price: $scope.price,
                      type: $scope.type
                  }
              )
          );
        };

        ws.onmessage = function (event) {
            var msg = JSON.parse(event.data);
            if (msg.act === 'result') {
                if(msg.data) {
                    showNoti('Операция успешно добавлена');
                }
                else {
                    showNoti('Ошибка добавления')
                }

                $scope.date = '';
                $scope.price = '';
            }
        };

        // Дата
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        $scope.toggleMin = function () {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
        $scope.toggleMin();
        $scope.open = function () {
            $scope.popup.opened = true;
        };
        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        $scope.format = 'dd.MM.yyyy';
        $scope.altInputFormats = ['dd.MM.yyyy'];
        $scope.popup = {
            opened: false
        };
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }
    });
function showNoti(msg) {
    if (Notification.permission === 'granted'){
        var notification = new Notification('ВсеАвто', {
            lang: 'ru-RU',
            body: msg,
            icon: '/favicon.ico'
        });
        setTimeout(notification.close.bind(notification), 3000);
    }
    else{
        alert(msg);
    }
};
