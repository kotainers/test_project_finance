/**
 * Created by kotainer on 14.10.2016.
 */
var Operation = require('../models/operation').Operation;
var WebSocketServer = new require('ws');

var webSocketServer = new WebSocketServer.Server({port: 8987});

webSocketServer.on('connection', function(ws) {
    ws.on('message', function(message) {
        try{
            console.log(message);
            var msg = JSON.parse(message);
            if (msg.act === 'addNew') {
                Operation.create(msg.date, msg.price, msg.type, function (res){
                    console.log(res);
                    ws.send(JSON.stringify(
                        {
                            act: "result",
                            data: res
                        })
                    )
                });
            }
            // Получить весь список элементов
            if (msg.act === 'getAll') {
                Operation.getList(function (promise) {
                    promise.then(function(arrayOfOperations) {
                        ws.send(JSON.stringify(
                            {
                                act: 'fullList',
                                data: arrayOfOperations
                            })
                        );
                    });
                });
            }
        }
        catch(e) {
            console.error(e);
        }
    });

    ws.on('close', function() {
        delete  ws;
    });
});