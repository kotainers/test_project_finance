var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var opts = {
    "server": {
        "socketOptions": {
            "keepAlive": 1
        }
    }
};
mongoose.connect('mongodb://127.0.0.1/financeOperations',opts);

module.exports = mongoose;
