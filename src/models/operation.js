var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    date: {
        type: Date,
        default: Date.now
    },
    income: {
        type: Number,
        default: 0
    },
    outcome: {
        type: Number,
        default: 0
    },
    profit: {
        type: Number,
        default: 0
    }
});

// создание нового элемента
schema.statics.create = function(d, price, type, callback){
    var Op = this;
    var income = 0;
    var outcome = 0;

    var profit = price/100*2;

    //Определяем тип операции
    if (type == 0) {
        income = price;
    }
    else {
        outcome = price;
    }

    var oper = new Op({
        date:d,
        income: income,
        outcome:outcome,
        profit:profit
    });

    oper.save(function (err) {
        if(err) {
            return callback(false)
        }
        else {
            return callback(true);
        }
    });
};

// получение всех
schema.statics.getList = function(callback){
    var Op = this;
    return callback(Op.find({}).exec());
};

exports.Operation = mongoose.model('Operation', schema);
