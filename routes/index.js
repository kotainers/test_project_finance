var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Управления финансами', path:'index' });
});

router.get('/add', function(req, res, next) {
  res.render('add', { title: 'Управления финансами | Новая операция', path:'add' });
});

module.exports = router;
